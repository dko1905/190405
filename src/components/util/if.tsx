import { Slot, component$ } from "@builder.io/qwik"

export const If = component$((props: { case: boolean }) => {
  return <>{props.case && <Slot />}</>;
});
