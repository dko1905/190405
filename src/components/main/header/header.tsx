import { component$ } from "@builder.io/qwik";
import styles from "./header.module.css";
import { Link } from "@builder.io/qwik-city";

export default component$<{
  title: string,
  subtitle: string
}>(({title, subtitle}) => {
  return (
    <header class={styles.header}>
      <h1>{title}</h1>
      <h2>{subtitle}</h2>
      <nav>
        <Link href="/">Home</Link>
        <Link href="/about">About</Link>
      </nav>
    </header>
  );
});
