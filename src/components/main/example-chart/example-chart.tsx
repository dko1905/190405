import { component$, useSignal, useVisibleTask$ } from "@builder.io/qwik";
import { Chart, registerables } from "chart.js";
import styles from "./index.module.css";

export const ExampleChart = component$(() => {
  const chart = useSignal<HTMLCanvasElement>();

  const data = {
    labels: ["Label 1", "Label 2", "Label 3"],
    datasets: [
      {
        label: "Demo chart",
        data: [300, 50, 100],
        backgroundColor: ["#7000e1", "#fc8800", "#00b0e8"],
        // hoverOffset: 4,
        borderWidth: 0,
      },
    ],
  };
  const config = {
    type: "doughnut",
    data: data,
    options: {
      borderRadius: "30",
      responsive: false,
      cutout: "95%",
      spacing: 2,
      plugins: {
        legend: {
          position: "bottom",
          display: true,
          labels: {
            usePointStyle: true,
            padding: 20,
            font: {
              size: 14,
            },
          },
        },
        title: {
          display: true,
          text: "Example chart",
        },
      },
    },
  };

  // eslint-disable-next-line qwik/no-use-visible-task
  useVisibleTask$(() => {
    Chart.register(...registerables);
    new Chart(chart.value!, config as any);
  });

  return (
    <>
      <div class={styles.canvasw}>
        <canvas ref={chart}></canvas>
      </div>
    </>
  );
});
