import { component$, Slot, useStyles$ } from "@builder.io/qwik";
import { type RequestHandler, useDocumentHead } from "@builder.io/qwik-city";
import { QwikCityNprogress } from "@quasarwork/qwik-city-nprogress";

import Header from "~/components/main/header/header";
import Footer from "~/components/main/footer/footer";

import styles from "./styles.css?inline";

export const onGet: RequestHandler = async ({ cacheControl }) => {
  // Control caching for this request for best performance and to reduce hosting costs:
  // https://qwik.builder.io/docs/caching/
  cacheControl({
    // Always serve a cached response by default, up to a week stale
    staleWhileRevalidate: 60 * 60 * 24 * 7,
    // Max once every 5 seconds, revalidate on the server to get a fresh version of this page
    maxAge: 5,
  });
};

export default component$(() => {
  const head = useDocumentHead();

  useStyles$(styles);
  return (
    <>
      <Header subtitle={head.title} title="190405" />
      <QwikCityNprogress />
      <main>
        <Slot />
      </main>
      <Footer />
    </>
  );
});

export const GetRoute = (path: string) => {
  switch (path) {
    case "/":
      return { name: "Home" };
    case "/about/":
      return { name: "About" };
    default:
      return { name: "Unknown" };
  }
};
