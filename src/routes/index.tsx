import { component$ } from "@builder.io/qwik";
import { Link, type DocumentHead } from "@builder.io/qwik-city";

export default component$(() => {
  return (
    <>
      <section>
        <h3>Links</h3>
        <ul>
          <li>
            <Link href="/page/dart-encoder-rewrite-web/">link</Link>
            {" - "}
            <a href="https://github.com/dko1905/dart-projects">GitHub</a>
            {" - "} Custom "encryption" program written in Dart.
          </li>
          <li>
            <Link href="/page/turgay-fysik-side/">link</Link>
            {" - "}
            <a href="https://gitlab.com/dko1905/small-projects">GitLab</a>
            {" - "} Turgays Fysik Side v3.3
          </li>
        </ul>
      </section>
      <section>
        <h3>Andre links</h3>
        <ul>
          <li>
            Turgays Fysik Side
            <a href="https://bafybeie2pgnv4d7k3scptrqk7v3vsgugn2tetcjvbmca36lty6akoixfyq.ipfs.cf-ipfs.com/">
              {" "}
              [Cloudflare Gateway v3.3]
            </a>
            <a href="https://bafybeif7726eqdyvnx2m4hsxdnaqbexttytvcw3ou33p7yrgqzykuino5q.ipfs.cf-ipfs.com/">
              {" "}
              [Cloudflare Gateway v3.2]
            </a>
            <a href="https://bafybeia52n7hlz5bw2j36uuafg5skyrlma5ibxebsrav4mykll4i5vz66y.ipfs.cf-ipfs.com/">
              {" "}
              [Cloudflare Gateway v2]
            </a>
            <a href="https://bafybeifkzw5v3mzjjostci5yqyetjog2qr2cbrwsyxj5xgqy6eip45klfq.ipfs.cf-ipfs.com/">
              {" "}
              [Cloudflare Gateway v1]{" "}
            </a>
          </li>
        </ul>
      </section>
    </>
  );
});

export const head: DocumentHead = {
  title: "Home",
  meta: [
    {
      name: "description",
      content: "190405.xyz webpage",
    },
  ],
};
