import { $, component$, useSignal, useVisibleTask$ } from "@builder.io/qwik";
import type { DocumentHead } from "@builder.io/qwik-city";

const importLib = () =>
  // @ts-ignore
  import("https://blobs.190405.xyz/dart-encoder-rewrite-web/test.js");

export default component$(() => {
  const loading = useSignal(true);
  const inputStr = useSignal("");
  const outputStr = useSignal("");
  const errorLog = useSignal("");

  const enc = $(async (s: string) => {
    const mod = await importLib();
    try {
      return mod.default.toBase32Str(s);
    } catch (e) {
      console.error(e);
      errorLog.value += `${e}\n`;
      return "";
    }
  });

  const dec = $(async (s: string) => {
    const mod = await importLib();
    try {
      return mod.default.toBase10Str(s);
    } catch (e) {
      console.error(e);
      errorLog.value += `${e}\n`;
      return "";
    }
  });

  // eslint-disable-next-line qwik/no-use-visible-task
  useVisibleTask$(async () => {
    await importLib();
    loading.value = false;
  });

  return (
    <>
      {loading.value ? (
        <p>Loading...</p>
      ) : (
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            gap: "1rem",
            maxWidth: "500px",
          }}
        >
          <label>
            Input: <br />
            <textarea style={{ width: "99%" }} rows={2} bind:value={inputStr} />
          </label>
          <div style={{ display: "flex", gap: "2px" }}>
            <button
              onClick$={async () => {
                outputStr.value = await enc(inputStr.value);
              }}
            >
              Encrypt
            </button>
            <button
              onClick$={async () => {
                outputStr.value = await dec(inputStr.value);
              }}
            >
              Decrypt
            </button>
          </div>

          <label>
            Output: <br />
            <textarea
              style={{ width: "99%" }}
              rows={2}
              bind:value={outputStr}
            />
          </label>
          <label>
            Errors: <br />
            <textarea
              style={{ width: "99%" }}
              rows={15}
              readOnly={true}
              value={errorLog.value}
            />
          </label>
        </div>
      )}
    </>
  );
});

export const head: DocumentHead = {
  title: "dart-encoder-rewrite-web",
};
