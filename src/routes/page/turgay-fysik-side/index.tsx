import { component$ } from "@builder.io/qwik";
import type { DocumentHead } from "@builder.io/qwik-city";

const IFRAME_URL = "https://blobs.190405.xyz/turgay-fysik-side/v3.3/index.html";

export default component$(() => {
  return (
    <>
      <p>
        Kan også findes <a href={IFRAME_URL}>her</a>.
      </p>
      <iframe style={{ height: "100vh", width: "90%" }} src={IFRAME_URL} />
    </>
  );
});

export const head: DocumentHead = {
  title: "Turgay Fysik side",
};
