import { component$, useSignal, useVisibleTask$ } from "@builder.io/qwik";
import { type DocumentHead } from "@builder.io/qwik-city";
import { ExampleChart } from "~/components/main/example-chart/example-chart";

export default component$(() => {
  const isLoading = useSignal(true);

  // eslint-disable-next-line qwik/no-use-visible-task
  useVisibleTask$(({ cleanup }) => {
    // Load chart at runtime
    const h = setTimeout(() => {
      isLoading.value = false;
    }, 100);

    cleanup(() => {
      clearTimeout(h);
    });
  });

  return (
    <>
      <p>About</p>
      Nothing, check <a href="https://0chaos.eu/">0chaos.eu</a>
      <br /> <br />
      {isLoading.value ? <p>Loading...</p> : <ExampleChart />}
    </>
  );
});

export const head: DocumentHead = {
  title: "About",
};
